package de.audioattack.openlink.logic;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ToolTest {

    @Test
    public void testSimpleUrlInText() {

        final String input = "blah blah http://example.org blah blah";
        final Set<String> output = toSet("http://example.org");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testSimpleUrlWithTrailingSlashInText() {

        final String input = "blah blah http://example.org/ blah blah";
        final Set<String> output = toSet("http://example.org/");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testSimpleUrlsInText() {

        final String input = "blah blah http://example.org\n\nhttp://www.example.net blah blah";
        final Set<String> output = toSet("http://example.org", "http://www.example.net");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testUrlWithPathInText() {

        final String input = "blah blah http://example.org/test/test.html blah blah";
        final Set<String> output = toSet("http://example.org/test/test.html");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testUrlsWithPathInText() {

        final String input = "blah blah http://example.org/test/test.html blah http://example.net/html/index.pl blah";
        final Set<String> output = toSet("http://example.org/test/test.html", "http://example.net/html/index.pl");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testIncompleteSimpleUrlInText() {

        final String input = "blah blah example.org blah blah";
        final Set<String> output = toSet("http://example.org");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testIncompleteSimpleUrlsInText() {

        final String input = "blah blah example.org blah example.net blah";
        final Set<String> output = toSet("http://example.org", "http://example.net");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testIncompleteUrlsInText() {

        final String input = "blah blah example.org blah blah";
        final Set<String> output = toSet("http://example.org");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testWikipediaUrlInText() {

        final String input = "blah blah https://de.wikipedia.org/wiki/Walter_Bode_(Politiker) blah blah";
        final Set<String> output = toSet("https://de.wikipedia.org/wiki/Walter_Bode_(Politiker)");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    @Test
    public void testNotWikipediaUrlInText() {

        final String input = "blah blah (home: https://example.org/) blah blah";
        final Set<String> output = toSet("https://example.org/");

        Assert.assertEquals(output, Tool.getUris(input));
    }

    private static Set<String> toSet(final String... strings) {
        return new HashSet<>(Arrays.asList(strings));
    }
}
